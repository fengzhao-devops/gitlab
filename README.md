## gitlab 简介


GitLab是利用Ruby on Rails一个开源的版本管理系统，实现一个自托管的Git项目仓库，可通过Web界面进行访问公开的或者私人项目。使用Git作为代码管理工具，并在此基础上搭建起来的web服务。


Gitlab 不单单是一个代码托管的 VCS 版本控制系统，其他例如 CI/CD Pipelie，Wiki，Issue Tracking，Kanban 等等都是极少数人所知，但却可以极大提高软件的开发效率及部署效率的。它支持相关的个性化设置和配置，同时gitlab 支持相关的 CI （持续化集成）， 为相关的项目自动化集成构建、测试、部署、交付提供了可能。

GitLab 分为两个版本，GitLab CE（社区版）及 GitLab EE（企业版），其中 GitLab CE 为 MIT 许可证可以免费进行使用，而 GitLab EE 则为在 GitLab CE 的版本上增强了许多功能，但需要进行付费使用。

无论是 GitLab CE 或者 GitLab EE 都可以选择自行托管私服务器私有部署，或者是可以直接选择 gitlab.com 的 SaaS 服务，gitlab.com 目前也提供免费的套餐，有需要的话可以随时进行升级计划。

omnibus方式即是通过gitlab提供的一个打包好的文件（将gitlab的所有服务打包在一起），通过类似一键似的安装来完成gitlab-ce企业私有代码库的安装，也是安装最省力的方式。



## sourcetree 

SourceTree 是 Windows 和Mac OS X 下免费的 Git 客户端，拥有可视化界面，容易上手操作。

同时它也是Mercurial和Subversion版本控制系统工具。支持创建、提交、clone、push、pull 和merge等操作。


