# gitlab安装指南


GITLAB支持的操作系统：

- Ubuntu (16.04/18.04/20.04)
- Debian (9/10)
- CentOS (6/7/8)
- openSUSE (Leap 15.1/Enterprise Server 12.2)
- Red Hat Enterprise Linux (please use the CentOS packages and instructions)
- Scientific Linux (please use the CentOS packages and instructions)
- Oracle Linux (please use the CentOS packages and instructions)


硬件要求：

- CPU要求：

    - 4 cores is the recommended minimum number of cores and supports up to 500 users
    - 8 cores supports up to 1000 users



- 内存要求


    - 从13.0起，gitlab的后端web服务从Unicorn变成了Puma（一个基于ruby的，简单快速，多核，更好的httpserver）
